package it.marco.develop.goosegame;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import it.marco.develop.goosegame.logic.HandleCommands;
import it.marco.develop.goosegame.utils.VictoryException;
import junit.framework.TestCase;

public class AppTest extends TestCase {

    public AppTest( String testName ){
        super( testName );
    }

    @Test
    public void testApp(){
    	List<String> moves = new ArrayList<String>();
    	moves.add("add player pippo");
    	moves.add("move pippo 6, 6");
    	moves.add("move pippo 6, 6");
    	moves.add("move pippo 6, 6");
    	moves.add("move pippo 6, 6");
    	moves.add("move pippo 6, 6");
    	moves.add("move pippo 1, 2");
    	try {
    		for(String token : moves) {
				HandleCommands.parseCommand(token);
			} 
    	} catch (VictoryException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertTrue(false);
		}
    	assertTrue(false);
    }
}
