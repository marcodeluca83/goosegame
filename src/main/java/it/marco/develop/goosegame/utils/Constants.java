package it.marco.develop.goosegame.utils;

public class Constants {

	public final static String playerAdded = "players: ";
	public final static String playerExisting = ": already existing player";
	
	public final static String rolls = " rolls ";
	public final static String movesFrom = " moves from ";
	public final static String to = "to ";
	
	public final static String wins = " Wins!!";
	public final static String wrongPlayer = "wrong player on move action";
	public final static String wrongDice = "wrong dice on move action";
	
	public final static String addPlayer = "add player ";
	public final static String move = "move ";
}
