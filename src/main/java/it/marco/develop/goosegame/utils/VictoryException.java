package it.marco.develop.goosegame.utils;

public class VictoryException extends Exception{

	private static final long serialVersionUID = 1L;
	
    public String message;

    public VictoryException(String message){
        this.message = message;
    }

    @Override
    public String getMessage(){
        return message;
    }

}
