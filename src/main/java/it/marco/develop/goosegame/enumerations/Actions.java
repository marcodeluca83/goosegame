package it.marco.develop.goosegame.enumerations;

public enum Actions {

	ADD_PLAYER("add player"),
	MOVE("move");
	
	private String action;
	
	Actions(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }
}
