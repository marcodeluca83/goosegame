package it.marco.develop.goosegame;

import java.util.Scanner;

import it.marco.develop.goosegame.logic.HandleCommands;

public class App {
	
    public static void main(String args[]){
    	
        Scanner in = new Scanner(System.in);
        
        while(true) {
            String s = in.nextLine();
            
            try {
				if(!HandleCommands.parseCommand(s))
					break;
			} catch (Exception e) {
				System.out.println(e.getMessage());
				break;
			}
            
        }
        
        in.close();

    }
}
