package it.marco.develop.goosegame.entities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import it.marco.develop.goosegame.utils.Constants;
import it.marco.develop.goosegame.utils.VictoryException;

public class Board {

	private Map<Player,Position> playersPositions;
	private Integer winSpace = 63;
	private Integer theBridge = 6;
	private List<Integer> gooseList = Arrays.asList(5, 9, 14, 18, 23, 27);
	
	public Board() {
		playersPositions = new LinkedHashMap<Player,Position>();
	}
	
	public String addPlayer(String val) {
		Player toAdd = new Player(val);
		
		if(playersPositions.containsKey(toAdd))
			return val + Constants.playerExisting;
		
		playersPositions.put(toAdd, new Position());
		return Constants.playerAdded + printPlayers();
	}
	
	public String move(String player, String dice1, String dice2) throws VictoryException {
		Player toMove = new Player(player);
		
		if(!playersPositions.containsKey(toMove))
			return Constants.wrongPlayer;
		
		Position pos = playersPositions.get(toMove);
		Integer oldPos = pos.getCurrent();
		sumDices(pos, Integer.valueOf(dice1), Integer.valueOf(dice2));
		
		playersPositions.put(toMove, pos);
		String eval = player + " rolls " + dice1 + ", " + dice2 + ". " + player + " moves from "
				+ evalBonusSpaces(oldPos) + " to " + evalBonusSpaces(pos.getCurrent());
		
		eval = verifyPrank(pos, player, eval);
		
		return verifyExtraConditions(pos, Integer.valueOf(dice1), Integer.valueOf(dice2), eval, player);
	}

	public Map<Player, Position> getPlayersPositions() {
		return playersPositions;
	}

	public void setPlayersPositions(Map<Player, Position> playersPositions) {
		this.playersPositions = playersPositions;
	}
	
	private String printPlayers() {
		List<Player> list = new ArrayList<Player>(playersPositions.keySet());
		return String.join(", ", list.stream().map(i -> i.getName()).collect(Collectors.toList()));
	}
	
	private void sumDices(Position pos, Integer dice1, Integer dice2) {
		Integer oldCurrent = pos.getCurrent();
		pos.setPrevious(oldCurrent);
		pos.setCurrent(oldCurrent + dice1 + dice2);
	}
	
	private String verifyExtraConditions(Position pos, Integer dice1, Integer dice2, 
			String eval, String player) throws VictoryException {
		
		Boolean condition = Boolean.TRUE;
		
		while(condition) {
			
			if(pos.getCurrent().equals(winSpace)) {
				eval = eval + ". " + player + " Wins!!";
				throw new VictoryException(eval);
			} else if(pos.getCurrent() > winSpace) {
				sumDices(pos, (winSpace-pos.getCurrent())*2, 0);
				eval = eval + ". " + player + " bounces! " + player + "returns to " + pos.getCurrent();
				condition = Boolean.FALSE;
			} else if(pos.getCurrent().equals(theBridge)) {
				sumDices(pos, 6, 0);
				eval = eval + ". " + player + " jumps to " + pos.getCurrent();
				condition = Boolean.FALSE;
			} else if(gooseList.contains(pos.getCurrent())) {
				sumDices(pos, dice1, dice2);
				eval = eval + ", The Goose. " + player + " moves again and goes to " + pos.getCurrent();
			} else
				condition = Boolean.FALSE;
			
		}
		
		return eval;
	}
	
	private String verifyPrank(Position pos, String player, String eval) {
		
		Player toPrank = null;
		Position posToPrank = null;
		
		for(Player iter : playersPositions.keySet()) {
			if(iter.equals(new Player(player)))
				continue;
				
			Position iterPos = playersPositions.get(iter);
			if(iterPos.getCurrent().equals(pos.getCurrent())) {
				toPrank = iter;
				posToPrank = iterPos;
				break;
			}
		}
		
		if(null != toPrank) {
			posToPrank.setPrevious(posToPrank.getCurrent());
			posToPrank.setCurrent(pos.getPrevious());
			
			eval = eval + ". On " + pos.getCurrent() + " there is " + toPrank.getName() + ", who returns to " + posToPrank.getCurrent();
		}
		
		return eval;
	}
	
	private String evalBonusSpaces(Integer val) {
		if(val.equals(0))
			return "Start";
		if(val.equals(theBridge))
			return "The Bridge";
		return String.valueOf(val);
	}
	
}
