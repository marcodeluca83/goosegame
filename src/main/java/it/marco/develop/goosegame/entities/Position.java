package it.marco.develop.goosegame.entities;

public class Position {

	private Integer current = 0;
	private Integer previous = null;
	
	public Integer getCurrent() {
		return current;
	}
	public void setCurrent(Integer current) {
		this.current = current;
	}
	public Integer getPrevious() {
		return previous;
	}
	public void setPrevious(Integer previous) {
		this.previous = previous;
	}
	
}
