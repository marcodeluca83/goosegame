package it.marco.develop.goosegame.logic;

import it.marco.develop.goosegame.entities.Board;
import it.marco.develop.goosegame.utils.Constants;

public class HandleCommands {
	
	private static Board board = new Board();

	public static Boolean parseCommand(String token) throws Exception {
		
		if(token.startsWith(Constants.addPlayer)) {
			handleAdd(token);
		} else if(token.startsWith(Constants.move)) {
			handleMove(token);
		} else {
			System.out.println("Wrong request, game over.");
			return Boolean.FALSE;
		}
		
		return Boolean.TRUE;
	}
	
	private static void handleAdd(String token) throws Exception {
		token = token.substring(Constants.addPlayer.length());
		
		if(token.contains(" "))
			throw new Exception("Wrong adding player");
		
		System.out.println(board.addPlayer(token));
	}
	
	private static void handleMove(String token) throws Exception {
		token = token.substring(Constants.move.length());
		
		String player = null;
		String dice1 = null;
		String dice2 = null;
		
		if(token.matches("[a-z0-9_-]+")) {
			player = token;
			dice1 = String.valueOf((int)(Math.random() * 6 + 1));
			dice2 = String.valueOf((int)(Math.random() * 6 + 1));
		} else if (token.matches("[a-z0-9_-]+[\\s]{1}[1-6],[\\s]{1}[1-6]")) {
			String [] arrMoving = token.split(" ");
			
			player = arrMoving[0];
			dice1 = arrMoving[1].replace(",", "");
			dice2 = arrMoving[2];
		} else 
			throw new Exception("Wrong move syntax");
		
		System.out.println(board.move(player, dice1, dice2));
	}
	
}
